const axios = require('axios');

const getClima = async (lat, lng) => {
    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=bb9683e10a0453c4733edef1c877f6a6&units=metric`);

    return resp.data.main.temp;
};

module.exports = {
    getClima
}